#ifndef _GL_UTILS_h
#define _GL_UTILS_h

#define DEFAULT_UNIFORM_TIME 0.0

#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/gl.h>

struct shader_data {
	float rate;
	GLuint rateID;
	GLuint program;
};

struct shader_uniforms_t {
	GLfloat time; 
	GLfloat end; 
	GLfloat failure_time; 
};

Window fullscreen_win(Display*, Window);
void create_gl_context(Display*, Window);
GLuint setup_shaders(Display* xdisplay, Window root, Window win,
		     struct shader_data* data, const char* fragment_shader_file);
float elapsed_time_s(struct timespec start_time);
void update_uniforms(struct shader_uniforms_t *values, struct shader_data *data);
void default_uniforms(struct shader_uniforms_t *uniforms);

#endif /* _GL_UTILS_h */


