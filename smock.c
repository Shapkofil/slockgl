/*
Conviniently develop shaders for the slock GL patch.
Author: Kiril Iliev<ldshapkofil@gmail.com>
 */

#include <X11/X.h>
#define _XOPEN_SOURCE 500
#define DEBUG 1
#if HAVE_SHADOW_H
#include <shadow.h>
#endif


#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <X11/extensions/Xrandr.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "gl_utils.h"
#include "arg.h"

char *argv0;

static void
die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);
	exit(1);
}

static void
usage(void)
{
	die("usage: slock [-vs] [cmd [arg ...]]\n"
	    "  -s (required) fragment shader path");
}

Window
create_smock_window(Display *dpy, Window root){
	XVisualInfo *vi;
	XSetWindowAttributes swa;

	GLint att[] = { GLX_RGBA,
			GLX_DEPTH_SIZE, 24,
			GLX_DOUBLEBUFFER, None };

	if ((vi = glXChooseVisual(dpy, 0, att)) == NULL){
		die("GLX Visual failed to create");
	}
	swa.colormap = XCreateColormap(dpy,
				       root,
				       vi->visual,
				       AllocNone);
	swa.event_mask = ExposureMask | KeyPressMask;


	return XCreateWindow(dpy,
			     root,
			     0, 0,
			     640, 480,
			     0, vi->depth,
			     InputOutput,
			     vi->visual,
			     CWColormap | CWEventMask,
			     &swa);
}

int
eventloop(Display *dpy, Window win, struct shader_data *shader_data){
	int running, started;
	unsigned long ksym;
	Atom wm_delete_window;
	XEvent xev;
	struct shader_uniforms_t uniforms;
	struct timespec start_time;

	wm_delete_window = XInternAtom(dpy, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(dpy, win, &wm_delete_window, 1);

	running = 1;
	started = 0;
	default_uniforms(&uniforms);

	while (running) {
	while (XPending(dpy)){
		XNextEvent(dpy, &xev);
		if (xev.type == KeyPress){
			ksym = XLookupKeysym(&xev.xkey, 0);
			switch (ksym) {
			case XK_l:
				clock_gettime(CLOCK_MONOTONIC_RAW,
					      &start_time);
				default_uniforms(&uniforms);
				started=1;
				break;
			case XK_u:
				default_uniforms(&uniforms);
				uniforms.end = elapsed_time_s(start_time);
				break;
			case XK_f:
				default_uniforms(&uniforms);
				uniforms.failure_time = elapsed_time_s(start_time);
				break;
			}
		} else if (xev.type == ClientMessage) {
			if((Atom) xev.xclient.data.l[0] == wm_delete_window) {
				printf("Window Manager killed window");
				running = 0;
			}
		}}
		uniforms.time = !started
			? 0
			: elapsed_time_s(start_time);
			
		update_uniforms(&uniforms, shader_data);
		// TODO: Is the program uniforms updated for the correct program
		glXSwapBuffers(dpy, win);
		glEnableVertexAttribArray(0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glDisableVertexAttribArray(0);
	}

	return 0;
}


int
main(int argc, char **argv){
	char *shader_file;
	Display *dpy;
	Window root, win;
	int eventloop_code;
	struct shader_data shader_data;

        if (argc <= 1)
	  usage();

	shader_file=(char *)0;
	ARGBEGIN {
	case 's':
	        shader_file=EARGF(usage());
	        break; 
	case 'v':
		puts("smock-"VERSION);
		return 0;
	default:
		usage();
	} ARGEND

	
	if (!(dpy = XOpenDisplay(NULL)))
		die("slock: cannot open display\n");

	root = XDefaultRootWindow(dpy);


	win = create_smock_window(dpy, root);
	XMapWindow(dpy, win);
	XStoreName(dpy, win, "smock");
	create_gl_context(dpy, win);
	shader_data.program = setup_shaders(dpy,
		root, win,
		&shader_data,
		shader_file);


	XSync(dpy,False);

	eventloop_code = eventloop(dpy, win, &shader_data);

	XCloseDisplay(dpy);
}
