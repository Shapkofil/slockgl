#version 330 core

// ---- gllock required fields -----------------------------------------------------------------------------------------
#define RATE 1.0

uniform float u_time;
uniform float u_end;
uniform float u_failure_time;
uniform sampler2D u_screentex;
uniform vec2 u_resolution;

// ---------------------------------------------------------------------------------------------------------------------
out vec4 FragColor;

#define FAILURE_DISPLACE 5.0f
#define FAILURE_DURATION .3f
#define FAILURE_FREQUENCY 5.0f

#define PI 3.14159265359


vec2
hash22(vec2 p) {
    p = vec2(dot(p, vec2(127.1, 311.7)), dot(p, vec2(269.5, 183.3)));
    return -1.0 + 2.0 * fract(sin(p) * 43758.5453123);
}

float
snoise(vec2 p) {
    vec2 i = floor(p);
    vec2 f = fract(p);

    // Smooth interpolation
    vec2 u = f * f * (3.0 - 2.0 * f);

    // Hash four corners and use only one component (e.g., x)
    float a = hash22(i + vec2(0.0, 0.0)).x;
    float b = hash22(i + vec2(1.0, 0.0)).x;
    float c = hash22(i + vec2(0.0, 1.0)).x;
    float d = hash22(i + vec2(1.0, 1.0)).x;

    // Interpolate between them
    return mix(mix(a, b, u.x), mix(c, d, u.x), u.y);
}

float
uiShake(float time, float duration, float amplitude, float frequency) {
    if (time > duration) return 0.0;

    float progress = time / duration;
    float decay = (1.0 - progress) * amplitude; // Decay over time

    // Horizontal shake
    return sin(progress * frequency) * decay;
}

float
fbm( vec2 x, in float H )
{    
	float t = 0.0;
	for( int i=0; i<3; i++ )
	{
		float f = pow( 2.0, float(i) );
		float a = pow( f, -H );
		t += a*snoise(f*x);
	}
	return t;
}

float
f_warp(vec2 p) {
	return fbm(p, .2);
}

vec2
twave(vec2 x, float freq) {
    float a = 1.0;
    return 2.0 * a * abs(2.0 * (x * freq - floor(0.5 + x * freq))) - a;
}

vec2
warp_domain(vec2 p, float disp, float fail){
	float t = .002 * u_time;
	vec2 q =  vec2(
		f_warp(p + vec2 (-1.2, 5.2) + vec2(-3.0, 5.4)*t),
		f_warp(p + vec2 (2.2, 1.2) + vec2(5.2, 1.3)*t)
		);
	q += 0.05 * sin(vec2(2.3, 1.2)*length(q) + fail);
	q = mix(p, q, disp);

	vec2 o =  vec2(
		f_warp(q + vec2 (4.1, -2.1) + vec2(-5.0, 22.1)*t),
		f_warp(q + vec2 (6.7, -0.3) + vec2(9.2, 3.1)*t)
		);
	o += 0.05 * sin(vec2(3.2, 2.1)*length(q) );
	o = mix(q, o, disp);

	return twave(p + disp*o, .25);
}

void
main(void) {
	float disp_time = smoothstep(0.0,1.0,(u_time-u_end)*RATE);
	disp_time = (u_end==0)?disp_time:(1.0-disp_time);
	float fail_time = u_failure_time == 0.0
		? 0.0
		: uiShake(u_time-u_failure_time,
			  FAILURE_DURATION,
			  FAILURE_DISPLACE,
			  FAILURE_FREQUENCY);
	vec2 uv = gl_FragCoord.xy / u_resolution.xy;

	vec3 col = texture2D(u_screentex, warp_domain(vec2(uv.x, 1.0 - uv.y), disp_time, fail_time)).rgb;
	// col = vec3(warp_domain(uv, disp_time, fail_time), 0.0);
	FragColor = vec4(col, 1.0);
}
//
