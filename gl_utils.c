/*
MIT/X Consortium License

Copyright for portions of gl_utils.c are held by Anselm R Garbe <anselm@garbe.us>, 2006-2012 as part of the project slock.
Copyright for portions of gl_utils.c are held by Kuravi Hewawasam <kuravih@gmail.com>, 2019. as part of the project gl_lock.
All other copyright for gl_utils.c are held by Kiril Iliev ldshapkofil@gmail.com, 2023.

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#include "gl_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <string.h>

#include <GL/glew.h>
#include <GL/glx.h>

#define DEBUG 1
#define DEBUG_UNIFORMS 1
#define MAX_N_TOKENS 65536
#define DELIMITERS " \t\r\n\a"


float
elapsed_time_s(struct timespec start_time)
{
	struct timespec tick;
	clock_gettime(CLOCK_MONOTONIC_RAW, &tick);
	if (tick.tv_nsec < start_time.tv_nsec)
		return (tick.tv_sec - 1.0f - start_time.tv_sec) + (1000000000.0f + tick.tv_nsec - start_time.tv_nsec)/1000000000.0f;
	else
		return (tick.tv_sec - start_time.tv_sec) + (tick.tv_nsec - start_time.tv_nsec)/1000000000.0f;
}

static GLuint
load_shaders_str(const char* vertex_shader_source, const char* fragment_shader_source)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	GLint Result = GL_FALSE;
	int InfoLogLength;

#if DEBUG
	printf("Compiling vertex shader\n");
	printf("------------------------- vertex shader -------------------------\n%s\n-----------------------------------------------------------------\n\n",vertex_shader_source);
#endif

	glShaderSource(VertexShaderID, 1, &vertex_shader_source , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if(InfoLogLength>0)
	{
		char* VertexShaderErrorMessage;
		VertexShaderErrorMessage = (char*) malloc(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
		free(VertexShaderErrorMessage);
	}

#if DEBUG
	printf("Compiling fragment shader\n");
	printf("------------------------ fragment shader ------------------------\n%s\n-----------------------------------------------------------------\n\n",fragment_shader_source);
#endif

	glShaderSource(FragmentShaderID, 1, &fragment_shader_source , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if(InfoLogLength>0)
	{
		char* FragmentShaderErrorMessage;
		FragmentShaderErrorMessage = (char*) malloc(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
		free(FragmentShaderErrorMessage);
		exit(1);
	}

	// Link the program
#if DEBUG
	printf("Linking program\n");
#endif
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if(InfoLogLength>0)
	{
		char* ProgramErrorMessage;
		ProgramErrorMessage = (char*) malloc(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
		free(ProgramErrorMessage);
	}

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

#if DEBUG
	printf("Shader Compilation Finished\n");
#endif

	return ProgramID;
}

static char*
read_file(const char* filename)
{
	char* buffer;
	long length = 0;
	size_t result = 0;
	FILE* fp = fopen(filename, "r");
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		length = ftell(fp);
		rewind(fp);
		buffer = (char*)malloc(length);
		if(buffer)
		{
			result = fread(buffer, 1, length, fp);
		}
		fclose(fp);
	}
	else
	{
#if DEBUG
		printf("file %s not loading\n",filename);
#endif
		exit(0);
	}
	
	if (result != length)
	{
#if DEBUG
		printf("Reading error %s\n",filename);
#endif
		exit(3);
	}
	return buffer;
}

static int
split_line (char* line, char*** p_tokens)
{
	char* temp_tokens[MAX_N_TOKENS];
	int i = 0, j = 0;
	char *token;
	int n_tokens = 0;

	token = strtok(line, DELIMITERS);
	while (token != NULL)
	{
		temp_tokens[i] = token;
		// printf("token : %s\n", temp_tokens[i]);
		i++;
		token = strtok(NULL, DELIMITERS);
	}
	n_tokens = i;

	// printf("----------\n");
	*p_tokens = (char**) malloc(n_tokens * sizeof(char*));
	for(j=0; j < n_tokens; j++)
	{
		// printf("token : %s\n", temp_tokens[j]);
		(*p_tokens)[j] = temp_tokens[j];
	}
	return n_tokens;
}

static void
shaders_defs(char* fragment_shader_source, struct shader_data* data)
{
	char** tokens;
	int n_tokens;
	size_t i;

	data->rate = 0.0f;
	n_tokens = split_line (fragment_shader_source, &tokens);
	for(i=0; i < n_tokens; i++)
	{
		if((strcmp(tokens[i],"#define")==0) && (strcmp(tokens[i+1],"RATE")==0))
		{
			data->rate = atof(tokens[i+2]);
		}
	}
#if DEBUG
	printf("shader data: rate = %f\n", data->rate);
#endif
	return;
}

static GLuint
load_shaders_file(const char* fragment_shader_file, struct shader_data* data)
{
	char *vertex_shader_str, *fragment_shader_str;
	GLuint ProgramID;

	vertex_shader_str = 
	  "#version 330 core\n"
	  "// Input vertex data, different for all executions of this shader.\n"
	  "layout(location = 0) in vec3 vertexPosition_modelspace;\n"
	  "\n"
	  "void main(void){\n"
	  "  gl_Position = vec4(vertexPosition_modelspace,1);\n"
	  "}";

	fragment_shader_str = read_file(fragment_shader_file);
#if DEBUG
	printf("------------------------ fragment shader ------------------------\nfile: %s\n-----------------------------------------------------------------\n\n",fragment_shader_file);
#endif
	ProgramID = load_shaders_str(vertex_shader_str, fragment_shader_str);

#if DEBUG
	printf("defs\n");
#endif
	shaders_defs(fragment_shader_str, data);


#if DEBUG
	printf("free the string\n");
#endif
	free(fragment_shader_str);
	return ProgramID;
}

Window
fullscreen_win(Display* xdisplay, Window root)
{
	XVisualInfo* xvisual_info;
	XWindowAttributes xwindow_attrib;
	XSetWindowAttributes set_xwindow_attrib;

	GLint attrib[] =
	{
		GLX_RGBA,
		GLX_DEPTH_SIZE, 24,
		GLX_DOUBLEBUFFER,
		None
	};
	xvisual_info = glXChooseVisual(xdisplay, 0, attrib);
	if(xvisual_info == NULL)
	{
		printf("no appropriate visual found\n");
		exit(0);
	}

	XGetWindowAttributes(xdisplay, root, &xwindow_attrib);

	set_xwindow_attrib.colormap = XCreateColormap(xdisplay, root, xvisual_info->visual, AllocNone);
	set_xwindow_attrib.event_mask = ExposureMask | KeyPressMask;
	set_xwindow_attrib.override_redirect = 1;

	return XCreateWindow(xdisplay,
			     root,
			     0, 0,
			     xwindow_attrib.width, xwindow_attrib.height,
			     0, xvisual_info->depth,
			     InputOutput,
			     xvisual_info->visual,
			     CWBorderPixel|CWColormap|CWEventMask|CWOverrideRedirect,
			     &set_xwindow_attrib );
}

static XImage*
screen_capture(Display* xdisplay, Window root, Window win)
{
	XWindowAttributes xwindow_attrib;
	XGetWindowAttributes(xdisplay, win, &xwindow_attrib);
	return XGetImage(xdisplay, root,
			 xwindow_attrib.x, xwindow_attrib.y,
			 xwindow_attrib.width, xwindow_attrib.height,
			 AllPlanes, ZPixmap);
}

#define MAJOR_VERSION 3
#define MINOR_VERSION 3

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);

void
create_gl_context(Display* xdisplay, Window win)
{
	int num_fbc;
	GLXFBConfig* xfbc;
	XVisualInfo* xvisual_info;
	GLXContext xcontext_old;
	glXCreateContextAttribsARBProc glXCreateContextAttribsARB;
	GLXContext xcontext;
	int major, minor;
	XWindowAttributes xwindow_attrib;

	static int xvisual_attribs[] =
	{
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
		GLX_DOUBLEBUFFER, 1,
		GLX_RED_SIZE, 1,
		GLX_GREEN_SIZE, 1,
		GLX_BLUE_SIZE, 1,
		None
	};
	
	xfbc = glXChooseFBConfig(xdisplay, DefaultScreen(xdisplay), xvisual_attribs, &num_fbc);
	if (!xfbc)
	{
		printf("glXChooseFBConfig() failed\n");
		exit(1);
	}
	
	// Create old OpenGL context to get correct function pointer for glXCreateContextAttribsARB()
	xvisual_info = glXGetVisualFromFBConfig(xdisplay, xfbc[0]);
	
	xcontext_old = glXCreateContext(xdisplay, xvisual_info, 0, GL_TRUE);

	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");
	/* Destroy old context */
	glXMakeCurrent(xdisplay, 0, 0);
	glXDestroyContext(xdisplay, xcontext_old);
	if (!glXCreateContextAttribsARB)
	{
		printf("glXCreateContextAttribsARB() not found\n");
		exit(1);
	}

	// Set desired minimum OpenGL version
	static int context_attribs[] =
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB, MAJOR_VERSION,
		GLX_CONTEXT_MINOR_VERSION_ARB, MINOR_VERSION,
		None
	};
	// Create modern OpenGL context
	xcontext = glXCreateContextAttribsARB(xdisplay, xfbc[0], NULL, 1, context_attribs);
	if (!xcontext)
	{
		printf("Failed to create OpenGL context. Exiting.\n");
		exit(1);
	}

	XMapRaised(xdisplay, win);
	glXMakeCurrent(xdisplay, win, xcontext);

	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	printf("OpenGL context created.\nVersion %d.%d\nVendor %s\nRenderer %s\n", major, minor, glGetString(GL_VENDOR), glGetString(GL_RENDERER));

	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		exit(0);
	}

	XGetWindowAttributes(xdisplay, win, &xwindow_attrib);
	glViewport(0, 0, xwindow_attrib.width, xwindow_attrib.height);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

GLuint
setup_shaders(Display* xdisplay, Window root, Window win, struct shader_data* data, const char* fragment_shader_file) 
{
	GLuint texture_id;
	GLuint VertexArrayID;
	GLuint programID;
	GLuint vertexbuffer;
	XWindowAttributes xwindow_attrib;
	XImage* ximage;

	XGetWindowAttributes(xdisplay, win, &xwindow_attrib);

	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	programID = load_shaders_file(fragment_shader_file, data);
	glUseProgram(programID);
	static const GLfloat g_vertex_buffer_data[] =
	{
		-1.0f,  1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f
	};
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER,
		     sizeof(g_vertex_buffer_data),
		     g_vertex_buffer_data,
		     GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

#if DEBUG
	printf("Capturing screen\n");
#endif
	ximage = screen_capture(xdisplay, root, win);
	if(ximage == NULL)
	{
		printf("\n\tximage could not be created.\n\n");
		exit(0);
	}

	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_MIN_FILTER,
			GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_MAG_FILTER,
			GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D,
		     0,
		     GL_RGB8,
		     xwindow_attrib.width,
                     xwindow_attrib.height,
		     0,
		     GL_BGRA,
		     GL_UNSIGNED_BYTE,
                     (void *)(&(ximage->data[0])));

        XDestroyImage(ximage);

	glUniform1i(glGetUniformLocation(programID, "u_screentex"), 0);
	glActiveTexture(GL_TEXTURE0);

	glUniform2f(glGetUniformLocation(programID, "u_resolution"),
		    xwindow_attrib.width, xwindow_attrib.height);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glClear( GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT ); // Clear the screen
	return programID;
}


_Static_assert(sizeof(struct shader_uniforms_t) == 12,
	       "update_uniforms is does not handle all uniforms");
void
update_uniforms(struct shader_uniforms_t *values, struct shader_data *data)
{
	GLuint prog;
	prog = data->program;

	glUniform1f(glGetUniformLocation(prog, "u_time"), values->time);
	glUniform1f(glGetUniformLocation(prog, "u_end"), values->end);
	glUniform1f(glGetUniformLocation(prog, "u_failure_time"), values->failure_time);
#if DEBUG_UNIFORMS
	printf("\n"
		"u_time \t\t =  %f \n"
		"u_end  \t\t = %f \n"
		"u_failure_time \t = %f\n"
	       "\n",
		values->time, values->end, values->failure_time
		);
#endif
}


_Static_assert(sizeof(struct shader_uniforms_t) == 12,
	       "default_uniforms does not handle all uniforms");
void
default_uniforms(struct shader_uniforms_t *uniforms){
	*uniforms = (struct shader_uniforms_t){
		.time=DEFAULT_UNIFORM_TIME,
		.end=DEFAULT_UNIFORM_TIME,
		.failure_time=DEFAULT_UNIFORM_TIME
	};
}
