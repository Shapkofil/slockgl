# slock - simple screen locker
# See LICENSE file for copyright and license details.

include config.mk

SRC = slock.c ${COMPATSRC} gl_utils.c
OBJ = ${SRC:.c=.o}

SMOCK_SRC = smock.c ${COMPATSRC} gl_utils.c
SMOCK_OBJ = ${SMOCK_SRC:.c=.o}

all: slock

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk arg.h util.h gl_utils.h

config.h:
	cp config.def.h $@

slock: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

smock: ${SMOCK_OBJ}
	${CC} -o $@ ${SMOCK_OBJ} ${LDFLAGS}

clean:
	rm -f slock smock ${OBJ} ${SMOCK_OBJ} slock-${VERSION}.tar.gz

dist: clean
	mkdir -p slock-${VERSION}
	cp -R LICENSE Makefile README slock.1 config.mk \
		${SRC} config.def.h arg.h util.h common.h slock-${VERSION}
	tar -cf slock-${VERSION}.tar slock-${VERSION}
	gzip slock-${VERSION}.tar
	rm -rf slock-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f slock ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/slock
	chmod u+s ${DESTDIR}${PREFIX}/bin/slock
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" <slock.1 >${DESTDIR}${MANPREFIX}/man1/slock.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/slock.1

install_tooling: smock
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f smock ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/smock
	chmod u+s ${DESTDIR}${PREFIX}/bin/smock

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/slock
	rm -f ${DESTDIR}${MANPREFIX}/man1/slock.1

.PHONY: all clean dist install uninstall
