/* user and group to drop privileges to */

static const char *user  = "nobody";
static const char *group = "nogroup";

// The screen will flash in between shader renders if this is false.
#define DISSABLE_COLORS 1
static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#005577",   /* during input */
	[FAILED] = "#CC3333",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
